import pprint
import os
from marketorestpython.client import MarketoClient
from methods import auth

if not os.path.exists('info'):
    os.makedirs('info')

smc = MarketoClient(**auth.sand_conn)

sandInfo = smc.execute(method="describe")

with open("info/mkto_sand_fields.txt", 'wt') as sOut:
    pprint.pprint(sandInfo, stream=sOut)



pmc = MarketoClient(**auth.sand_conn)

prodInfo = pmc.execute(method="describe")

with open("info/mkto_prod_fields.txt", 'wt') as pOut:
    pprint.pprint(prodInfo, stream=pOut)
