from marketorestpython.client import MarketoClient
from methods import auth
import pandas as pd
import argparse
import os
import glob
import time
import datetime
import csv
import random
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--instance", dest="instance", choices=['prod', 'sandbox'], default='sandbox',
                    help="what am I doing? Choices: split, start, resume, retry, check")

parser.add_argument("-m", "--mode", dest="mode", choices=['split', 'start', 'resume', 'retry', 'check'],
                    help="what am I doing? Choices: split, start, resume, retry, check")

parser.add_argument("-l", "--list", dest="listId",
                    help="the marketo list id you'd like to import to")

parser.add_argument("-f", "--file", dest="file_path",
                    help="full path of file you'd like to split and import", metavar="FILE")

args = parser.parse_args()

instance = args.instance

if instance =="prod":
    mc = MarketoClient(**auth.prod_conn)
else:
    mc = MarketoClient(**auth.sand_conn)

listId = args.listId

file_path = args.file_path

mode = args.mode

# =======================
# === PROCESSING VARS ===
# =======================

chunksize = 20000
wait = 10

# =======================
# =======================

def vlistDir(listId):
    listDir = "list_{}".format(listId)
    return listDir

def getDirs(listId):
    listDir = vlistDir(listId)
    errDir = "ERR"
    delayDir = "DELAY"
    completeDir = "COMPLETE"

    dirs = {
        "listDir" : listDir
        , "errDir" : errDir
        , "delayDir" : delayDir
        , "completeDir" : completeDir
    }

    return dirs

def creatDirs(listId):
    dirs = getDirs(listId)

    os.mkdir(dirs.get("listDir"))
    os.chdir(dirs.get("listDir"))
    os.mkdir(dirs.get("errDir"))
    os.mkdir(dirs.get("delayDir"))
    os.mkdir(dirs.get("completeDir"))

    return dirs

def splitFile(listId, file_path, chunksize):

    listDir = vlistDir(listId)
    print("{} | CREATING FILES FOR {}".format(datetime.datetime.now(), listDir))

    chunks=[]

    for i, chunk in enumerate(pd.read_csv(file_path, chunksize=chunksize)):
        csv_name = "chunk{}.csv".format(i)
        chunk.to_csv(csv_name, index=False)
        chunks.append(csv_name)

    return chunks

def importChunks(listId, dirs, wait, pref):
    listDir = vlistDir(listId)
    print("\r")
    print("{} | STARTING IMPORTS FOR {}".format(datetime.datetime.now(), dirs.get("listDir")))

    for chunkfile in glob.glob("{}chunk*.csv".format(pref)):
        baseChunkfile = os.path.basename(chunkfile)
        try:
            print("\r")
            print("{} | Importing {}".format(datetime.datetime.now(), baseChunkfile))

            impList = mc.execute(
                method="import_lead"
                , file=chunkfile
                , format="csv"
                ,listId=listId
                )

            impDict = next(iter(impList))

        except Exception as e:
            os.rename(
                chunkfile
                ,"{}/ERR_{}".format(dirs.get("errDir"), baseChunkfile)
                )
            print("{} | There was an Error:( {} ) with {}".format(datetime.datetime.now(), e,baseChunkfile))
            print("{} | Waiting {} Seconds then getting next chunk".format(datetime.datetime.now(), wait))

            time.sleep(wait)

        else:
            print("{} | Waiting {} Seconds then checking {} for completion".format(datetime.datetime.now(), wait, baseChunkfile))

            time.sleep(wait)
# ======
            try:
                batchId = impDict.get("batchId")

                batchList = mc.execute(
                    method='get_import_lead_status'
                    , id = batchId
                )

                batchDict = next(iter(batchList))

                status = batchDict.get("status")
                n = 0
                c = 10
                while (status != "Complete") and (n < c):
                    n = n + 1
                    sys.stdout.write("\r{} | Check {}/{} for {} seconds | Last Status: {}".format(datetime.datetime.now(), n, c, wait, status))

                    batchList = mc.execute(
                        method='get_import_lead_status'
                        , id = batchId
                    )

                    batchDict = next(iter(batchList))

                    status = batchDict.get("status")

                    time.sleep(wait)


            except:
                os.rename(
                    chunkfile
                    ,"{}/ERR_{}".format(dirs.get("errDir"), baseChunkfile)
                    )

                print("{} | Problem with {}".format(datetime.datetime.now(), baseChunkfile))

            else:
                if status =="Complete":
                    os.rename(
                        chunkfile
                        ,"{}/COMPLETED__{}".format(dirs.get("completeDir"), baseChunkfile)
                        )

                    print("\r")
                    print("{} | Completed {}".format(datetime.datetime.now(), baseChunkfile))

                else:
                    os.rename(
                        chunkfile
                        ,"{}/DELAY_status_{}__{}.csv".format(dirs.get("delayDir"), status, baseChunkfile)
                        )

                    print("\r")
                    print("{} | {} not complete | Status:{}".format(datetime.datetime.now(), baseChunkfile, status))


def startImport(listId, file_path, chunksize):
    dirs = creatDirs(listId)
    chunks = splitFile(listId, file_path, chunksize)
    importChunks(listId, dirs, wait,"")

def resumeImport(listId):
    dirs = getDirs(listId)
    os.chdir(dirs.get("listDir"))
    importChunks(listId, dirs, wait,"")

def retryImport(listId):
    dirs = getDirs(listId)
    listDir = dirs.get("listDir")
    os.chdir(dirs.get("listDir"))
    importChunks(listId, dirs, wait,"ERR/ERR_")


def checkDelayed(listId):
    dirs = getDirs(listId)
    listDir = dirs.get("listDir")
    os.chdir(listDir)

    print("\r")
    print("{} | Checking for List Membership".format(datetime.datetime.now()))

    for delayFile in glob.iglob('DELAY/DELAY*.csv'):
        baseDelayFile = os.path.basename(delayFile)
        print("\r")
        print("{} | checking random email from {}".format(datetime.datetime.now(), baseDelayFile))
        with open(delayFile) as f:
            reader = csv.reader(f)
            next(reader, None)
            chosen_row = random.choice(list(reader))

            zmail = next(iter(chosen_row))
            filterValues = [zmail]

            leads = mc.execute(
                method='get_multiple_leads_by_filter_type'
                , filterType='email'
                , filterValues=filterValues
                , fields=['id'], batchSize=None
                )

            leadsDict = next(iter(leads))

            leadId = leadsDict.get("id")

            listMember = mc.execute(
                method='member_of_list'
                , listId=listId
                , id=[leadId]
                )

            member = next(iter(listMember))
            baseDelayFile = os.path.basename(delayFile)

            if member.get("status") == "memberof":
                os.rename(
                    delayFile
                    ,"{}/COMPLETED_{}".format(dirs.get("completeDir"), baseDelayFile)
                    )

                print("{} | Completed {}".format(datetime.datetime.now(), delayFile))

            else:
                print("{} | {} not found in {} - moving file back for resume option".format(datetime.datetime.now(), baseDelayFile, listDir))

                os.rename(
                    delayFile
                    ,"chunked_{}".format(baseDelayFile)
                    )

if mode=="split":
    splitFile(listId, file_path, chunksize)

if mode=="start":
    startImport(listId, file_path, chunksize)

if mode=="resume":
    resumeImport(listId)

if mode=="retry":
    retryImport(listId)

if mode=="check":
    checkDelayed(listId)
