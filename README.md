# Bulk Importing into Marketo

## Requirements

For interacting with the Marketo API this project is using [marketo-rest-python](https://github.com/jepcastelein/marketo-rest-python), which is sparsely documented. I mostly just looked at their `client.py` file to get a sense of what I could do with it.

I did install marketo-rest-python with pip install.

`pip install marketorestpython`

other python libraries used are:
- pprint
- glob
- argparse
- keyring
- pandas
- requests

Most of this is in [requirements.txt](requirements.txt) which was generated with `pip freeze` but I think some of the libraries I'm using won't be there because the requirements were met with what I already had installed with python.

If this gets more widely used, it might be worth creating a more complete process for setting up a **virtualenv**

## Using this project

I've included `getInfo.py` which is run without arguments to retrieve field data from both sandbox and production Marketo instances. This will create an `info` directory and drop text files with python dictionaries there. If you want this more readable, you're welcome to submit a PR.

The CSV you are importing will have to have headers which are **identical** to the rest api field names in marketo. In my experience if you don't ensure that this is the case then Marketo will tell you that it imported the data, but won't do anything with the mislabelled columns. It just passively ignores them while you're thinking you're done **BUT YOU'RE NOT**.

I *highly* recommend using `getInfo.py` to make sure your CSV headers match the rest API field names.

### Configuring connections
I'm using keyring to store our client_secrets with encryption on my machine. There are other approaches that could work.

If you'd like to change which credentials are being used you'd need to change them in `methods/auth.py` which looks like this:

``` python
import keyring

prod_conn = {
    "munchkin_id" : "805-ptz-019",
    "client_id" : "742344d4-f0b6-48ef-9aec-a92d442f6153",
    "client_secret" : keyring.get_password("marketo", "secret")
}

sand_conn = {
    "munchkin_id" : "093-NND-132",
    "client_id" : "3bc31213-85fa-18e1-a2b8-c4bd44a4ac6e",
    "client_secret" : keyring.get_password("marketoSand", "secret")
}
```
** DO NOT WRITE THE CLIENT SECRET INTO `methods/auth.py`**

#### Using Keyring

First install keyring (if you haven't already)

`pip install keyring`

then you'll want to set the secret, which stores it in your systems password manager.

`keyring set marketo secret`

and

`keyring set marketoSand secret` respectively.

this will ask you for the password which would be the client_secret for the respective instance. You can get this from the Marketo webapp in the admin panel.

As written, `methods/auth.py` will retrieve the client_secret from where you've stored it in keyring if you use the snippets and naming above.

### Using the Import Utility

This utility is built assuming you have a local CSV you'd like to import and a listId of an existing Marketo list (so that the import can be found easily without creating extraneous fields in Marketo)

Work with Marketing to get a list created the listId is found in the URL. It's an integer with 2 character strings on each side.

![example](img/listIdex.png)

This listId would be 1308 in the above example

**`importUtil.py` takes the following arguments:**
- `--instance` (`-i`) which takes `prod` or `sandbox` and defaults to sandbox
- `--list` (`-l`) the marketo status listId into which you'd like to import your csv.
- `--file` (`-f`) the full path of the CSV you'd like imported
- `--mode` (`-m`) which determines what the script does and takes the following values
  - `split` - creates a new dir for the list you're importing and splits it into important friendly chunks
    - requires `--list` & `--file`
  - `start` - **Includes split** and imports the chunks into marketo using the listId
    - requires `--list` & `--file`
  - `resume` - if your import gets interrupted this will pick up the untouched chunks and import them.
    - requires `--list`
  - `retry`
    - If the script encounters errors it will move your files to the `ERR` directory in the created list directory. This will reprocess those chunks
  - `check`
    - If the script is unable to confirm a completed import it will put them in the `DELAY` directory in the created list directory. This will check to see if a random email from that chunk is found in the list. If it is, it will move the chunk to the completed directory, if not it will rename it `chunk_` for reprocessing, which can be done with `--mode=resume`

#### Script Tuning

Depending on the number of columns in your csv, its woth adjusting the chunksize and wait time in `importUtil.py`

``` python
# =======================
# === PROCESSING VARS ===
# =======================

chunksize = 20000
wait = 10

# =======================
# =======================
```

#### Example

Navigate to the `mkto-bulk-py` directory in your terminal

In this example I am importing into list

`python importUtil.py -m=start -l=1308 -f=/Users/jstark/js/analytics-scripts/mkto-bulk-py/test.csv`

if the script doesn't finish you can run
`python importUtil.py -m=resume -l=1308`

if I want to retry errored batches/chunks I'd run
`python importUtil.py -m=retry -l=1308`
