import keyring

prod_conn = {
    "munchkin_id" : "805-ptz-019",
    "client_id" : "742384d4-f0b7-44ef-9aec-a92d453f6153",
    "client_secret" : keyring.get_password("marketo", "secret")
}

sand_conn = {
    "munchkin_id" : "093-NND-132",
    "client_id" : "3bc31213-66fa-48e1-a2b8-c4bd66a4ac6e",
    "client_secret" : keyring.get_password("marketoSand", "secret")
}
